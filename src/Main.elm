module Main exposing (..)

import Svg
import Svg.Attributes exposing (..)


--------------------------  Sierpinski -----------------------


mkTriangle : Int -> Int -> Int -> Svg.Svg msg
mkTriangle lx ly side =
    let
        t_height =
            sqrt ((toFloat (side * side)) * 3.0 / 4.0)

        p1 =
            (toString lx) ++ "," ++ (toString ly)

        p2 =
            (toString (lx + side)) ++ "," ++ (toString ly)

        p3 =
            (toString (lx + (side // 2))) ++ "," ++ (toString (ly - (sqrt ((toFloat (side * side)) * 3.0 / 4.0) |> round)))
    in
        Svg.polyline [ points (p1 ++ " " ++ p2 ++ " " ++ p3 ++ " " ++ p1), fill "none", stroke "black" ] []



-- mkSierpinski : Int -> Int -> Int -> Int -> List (Svg.Svg msg)
-- mkSierpinski lx ly side depth =
--------------------------  Mini LOGO -----------------------


type alias TurtlePar =
    ( ( Int, Int ), Int )


{-| draw turtle on x, y, angle
-}
drawTurtle : Int -> Int -> Int -> Svg.Svg msg
drawTurtle posx posy angle =
    Svg.image [ xlinkHref "img/turtle.png", height "20", width "20", x (toString (posx - 10)), y (toString (posy - 10)), transform ("rotate(" ++ (toString (angle - 90)) ++ " " ++ (toString posx) ++ " " ++ (toString posy) ++ ")") ] []



-- interpret : TurtlePar -> List String -> List (Svg.Svg msg)
-- interpret turtlePar commands =


main =
    Svg.svg [ width "500px", height "500px", fill "blue" ] [ (drawTurtle 250 250 90) ]



--   Svg.svg [ width "500px", height "500px" ] (interpret ( ( 250, 250 ), 90 ) [ "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100" ])
