# Četrte vaje Elm@FRI

V četrtem tednu se spoznamo z risanjem SVG grafike.

## Trikotnik Sierpinskega

Narišite funkcije, ki vam bodo izrisale trikotnik Sierpinskega.

V datoteki `Main.elm` imate že definirano funkcijo:

```elm
mkTriangle : Int -> Int -> Int -> Svg.Svg msg
```
ki vam izriše en enakostraničen trikotnik, če mu podate xy koordinate levega spodnjega krajišča ter dolžino stranice.

S pomočjo te funkcije definirajte funkcijo
```elm
mkSierpinski : Int -> Int -> Int -> Int -> List (Svg.Svg msg)
```
ki ji podate koordinate spodnjega levega krajišča, dolžino stranice ter globino rekurzije za izris trikotnika Sierpinskega.

![](primer-sierpinski.png)


## Želvja grafika

Napišite interpreter za zelo enostavno podmnožico programskega jezika LOGO.

Spomnimo, LOGO je jezik, ki se uporablja za premikanje želvice po zaslonu. Želva pa med sprehodom za seboj pušča sled. Za to nalogo bomo poenostavili ukaze želvici na zgolj dva ukaza:
- premik naprej za določeno število pixlov,
- obrat za določeno število stopinj (pozitivno za obrat v desno, negativno za obrat v levo).


Želva bo predstavljena s parom, prvi element bo xy koordinata in drugi element naj bo kot (v stopinjah) za katerega je želvica obrnjena.
Ta par predstavimo s tipom:

```elm
type alias TurtlePar = ((Int,Int), Int)
```

Vaša naloga je napisati funkcijo:

```elm
interpret : TurtlePar -> List String -> List (Svg.Svg msg)
```
Ta funkcija prejme dva parametra:
- trenutno pozicijo želvice
- seznam (zaporedje) ukazov želvici
Vsak ukaz je podan kot niz, ki se začne z eno črko, za presledkom pa je še argument (celo število) temu ukazu.
Za premik naprej se ukaz začne na veliko črko F, za obrat pa veliko črko "R".
Primera:
- premik za 100 pixlov: `F 100`
- obrat za 90 stopinj : `R 90`

### Primer
Recimo za izris osemkotnika, bi pognali spodnji LOGO program:
```elm
interpret ( ( 250, 250 ), 90 ) [ "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100", "R 45", "F 100" ]
```

Prvi argument funkciji `interpret` je začetna pozicija želvice, zatem pa osem premikov in rotacij za 45 stopinj.
Kot rezultat bi dobili:

![](primer-logo.png)
